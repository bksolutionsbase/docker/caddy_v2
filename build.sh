#!/usr/bin/env bash

set -euxo pipefail


IMAGE_NAME="caddy_v2"
IMAGE_TAG=""
echo "Name:Tag= ${IMAGE_NAME}${IMAGE_TAG}"

FQ_IMAGE_NAME="${CI_REGISTRY_IMAGE}"
echo "FQIN= ${FQ_IMAGE_NAME}"

buildah bud -f ./Dockerfile -t ${FQ_IMAGE_NAME} .

buildah push ${FQ_IMAGE_NAME}

buildah push ${FQ_IMAGE_NAME} ${CI_REGISTRY_PATH_DOCKERHUB}/${IMAGE_NAME}${IMAGE_TAG}

if [[ -z "${IMAGE_TAG}" ]]; then
    buildah push ${FQ_IMAGE_NAME} ${CI_REGISTRY_PATH_DOCKERHUB}/${IMAGE_NAME}:latest
    latest="done"
fi
