FROM caddy:2-builder-alpine AS builder

RUN xcaddy build \
    --with github.com/caddy-dns/duckdns




FROM caddy:2-alpine

MAINTAINER Briezh Khenloo

# Set labels
LABEL name="briezh/caddy" \
      vendor="BKSolutions" \
      summary="Open source web server with automatic HTTPS" \
      description="Caddy 2 is a powerful, enterprise-ready, open source web server with automatic HTTPS written in Go." \
      picapport.run="docker run -rm --name caddy -p 8080:80 -v $PWD/data.d:/data -v $PWD/conf.d:/config -dt docker.io/briezh/caddy:latest" \
      picapport.docker.cmd="docker run -d -p 8080:80 -v $PWD/data.d:/data -v $PWD/conf.d:/config briezh/caddy:latest" \
      picapport.podman.cmd="podman run -d -p 8080:80 -v $PWD/data.d:/data:z -v $PWD/conf.d:/config:Z briezh/caddy:latest"

COPY --from=builder /usr/bin/caddy /usr/bin/caddy

LABEL version=latest \
      release=2-alpine
